package com.thomas.firstapp.dialogstudy;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    AlertDialog.Builder builder, builder2, builder3, builder4, builder5;
    String[] items = new String[]{"list1", "list2", "list3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initDialong();
        initListDialong();
        initSingleChoiceDialong();
        initMultiChoiceDialong();
    }

    private void initDialong() {
        builder = new AlertDialog.Builder(MainActivity.this);
        builder.setIcon(R.mipmap.ic_launcher);//아이콘
        builder.setTitle("DialongStudy");//타이틀
        builder.setMessage("Alert Dialog.....................omg");//내용
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() { //PositiveButton는 맨 오른쪽
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "yes", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNeutralButton("or", new DialogInterface.OnClickListener() {//NeutralButton는 맨 왼쪽
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "or", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {//NegativeButton은 맨 오른쪽 두번째
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "no", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setCancelable(false);//다이얼로그 취소막기
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {//취소됐을시
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Toast.makeText(MainActivity.this, "Dialong Canceled", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {//다이얼로그 종료시 호출
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Toast.makeText(MainActivity.this, "Dialong Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initListDialong() {
        builder2 = new AlertDialog.Builder(MainActivity.this);
        builder2.setIcon(R.mipmap.ic_launcher);
        builder2.setTitle("DialongList");
        builder2.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Toast.makeText(MainActivity.this, items[i], Toast.LENGTH_SHORT).show();
            }
        });
        builder2.setCancelable(false);//다이얼로그 취소막기
    }

    private void initSingleChoiceDialong() {
        builder3 = new AlertDialog.Builder(MainActivity.this);
        builder3.setIcon(R.mipmap.ic_launcher);
        builder3.setTitle("DialongSingleChoice");
        builder3.setSingleChoiceItems(items, 1, new DialogInterface.OnClickListener() {//체크할 숫자
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Toast.makeText(MainActivity.this, items[i], Toast.LENGTH_SHORT).show();
                dialogInterface.dismiss();//선택되면 사라지게
            }
        });
        builder3.setCancelable(false);//다이얼로그 취소막기
    }

    private void initMultiChoiceDialong() {
        builder4 = new AlertDialog.Builder(MainActivity.this);
        builder4.setIcon(R.mipmap.ic_launcher);
        builder4.setTitle("DialongMultiChoice");
        final boolean[] mSelected = new boolean[]{false, false, false};
        builder4.setMultiChoiceItems(items, mSelected, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                mSelected[i] = b;
            }
        });
        builder4.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < items.length; i++) {
                    if (mSelected[i]) {
                        sb.append(items[i] + ",");
                    }
                }
                Toast.makeText(MainActivity.this, sb.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initProgressDialong(boolean show) {
        builder5 = new AlertDialog.Builder(MainActivity.this);
        builder5.setView(R.layout.layout_progress);
        AlertDialog dialong =builder5.create();
        if(show){
            builder5.show();
        }else{
            dialong.dismiss();
        }

    }

    private void initProgressDialog(){//ProgressDialog대신 쓰자 (deprecated됨)
        AppCompatDialog dialog= new AppCompatDialog(MainActivity.this);
        //dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));//배경투명
        dialog.setContentView(R.layout.layout_progress);
        dialog.show();
        ProgressBar pro=(ProgressBar)dialog.findViewById(R.id.loader);//갖고올시
       // pro.setBackgroundColor(Color.BLUE);

    }


    @OnClick({R.id.dialogBasic, R.id.dialogList, R.id.dialogSingle, R.id.dialogMutl, R.id.dialogProgress,R.id.dialogFragment,R.id.dialogCustom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.dialogBasic:
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case R.id.dialogList:
                AlertDialog dialog2 = builder2.create();
                //ListView listView = dialog2.getListView();//리스트뷰갖고와서
                //listView.setAdapter();//어댑터 달기
                dialog2.show();
                break;
            case R.id.dialogSingle:
                AlertDialog dialog3 = builder3.create();
                dialog3.show();
                break;
            case R.id.dialogMutl:
                builder4.create().show();
                break;
            case R.id.dialogProgress:
                //initProgressDialong(true);
                initProgressDialog();
                break;
            case R.id.dialogFragment:
                MyDialogFragment dialogFragment = new MyDialogFragment();
                dialogFragment.show(getFragmentManager(),"dialog");
                break;
            case R.id.dialogCustom:
                CustomDialog dialog1 = new CustomDialog();
                dialog1.show(getSupportFragmentManager(),"custom");
                break;
        }
    }
}
