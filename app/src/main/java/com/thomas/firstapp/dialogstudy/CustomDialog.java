package com.thomas.firstapp.dialogstudy;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by thomas on 2017-12-11.
 */

public class CustomDialog extends DialogFragment{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL,R.style.MyDialog);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Dialog d= getDialog();//getDialog() 사용가능
        d.setTitle("정말 떠나실거에요 ? ㅠ3ㅠ");
        //d.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,R.mipmap.ic_launcher);
        //d.getWindow().setLayout(800,1800);//다이얼로그 사이즈 layout의 LinearLayout = wrap_content /RelativeLayout = match_parent

        //WindowManager.LayoutParams lp =d.getWindow().getAttributes();//정렬
       // lp.x = 150;
        //lp.y = 150;
        //lp.gravity= Gravity.LEFT | Gravity.TOP;

        //d.getWindow().setAttributes(lp);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Dialog d= getDialog();
        d.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        View v= inflater.inflate(R.layout.customdialog,container,false);
        Button btn =v.findViewById(R.id.btn_quit);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return v;
    }
}
