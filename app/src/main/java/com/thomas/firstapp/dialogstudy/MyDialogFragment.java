package com.thomas.firstapp.dialogstudy;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by thomas on 2017-11-23.
 */

public class MyDialogFragment extends DialogFragment{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle("dialong fragment");
        builder.setMessage("오예");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity(), "yes", Toast.LENGTH_SHORT).show();
                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment old =getFragmentManager().findFragmentByTag("dialog");
                if(old != null){
                    ft.remove(old);
                }
                ft.addToBackStack(null);
                MyDialogFragment dialog = new MyDialogFragment();
                dialog.show(getFragmentManager(),"dialog");
            }
        });
        return builder.create();
    }

}
